import { NumberValueAccessor } from '@angular/forms';

export interface VesselBookingStatistics {
  customerId: number;
  totalBookings: number;
  pendingApproval: number;
  approved: number;
  currentlySailing: number;
  onTrack: number;
  delayed: number;
}
