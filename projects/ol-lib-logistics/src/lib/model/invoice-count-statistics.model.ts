export interface InvoiceCountStatistics {
  totalInvoices;
  totalPaid;
  totalPartialPaid;
  totalUnpaid;
}
