import { OverSizedContainerDetails } from './over-sized-container-detail.model';

export interface SlotLossRequest {
  customerId?: number;
  polCode: string;
  podCode: string;
  oversizedContainerDetails: Array<OverSizedContainerDetails>;
  totalTeuLost?: number;
  slotLossAmountPerTeu?: number;
  totalSlotLossAmount?: number;
  currencyCode?: string;
}
// import { OverSizedContainerDetails } from "./over-sized-container-detail.model";

// export interface SlotLoss {

//   customerId?: number;
//   polCode?: string;
//   podCode?: string;
//   oversizedContainerQuantity?: string;
//   oversizedcontainerdetail?: Array<OverSizedContainerDetails>;
//   totalTeuLost?: number;
//   slotLossAmountPerTeu?: number;
//   totalSlotLossAmount?: number;
//   currencyCode?: string;
// }
