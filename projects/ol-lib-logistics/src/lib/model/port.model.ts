export interface Port {
  portId?: number;
  portCode?: string;
  portName?: string;
  countryCode?: string;
  countryName?: string;
  isActive?: boolean;
  createdOn?: Date;
}

// export interface Port {

//     portId?: number;
//     portCode?: string;
//     portName?: string;
//     countryCode?: string;
//     countryName?: string;
//     isActive?: boolean;
//     createdOn?: Date;
//   }
