export interface Commodity {
  commodityId?: number;
  code?: string;
  name?: string;
  createdOn?: Date;
}

// export interface Commodity {
//     commodityId?: number;
//     code?: string ;
//     name?: string;
//     createdOn?:Date;
// }
