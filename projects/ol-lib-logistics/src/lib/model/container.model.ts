export interface Container {
  id?: number;
  containerType?: string;
  containerCategory?: string;
  containerCode?: string;
  isLiner?: string;
  isCoastal?: string;
  cargoCategory?: string;
  otmContainerCode?: string;
}
// export interface Container {

//     id?: number;
//     containerType?: string;
//     containerCategory?: string;
//     containerCode?: string;
//     isLiner?: string;
//     isCoastal?: string;
//     cargoCategory?: string;
//     otmContainerCode?: string;

//   }
