export interface User {
  patchField?:string ;
  userId?: number;
  enabled?:boolean;
  newPassword?:string;
  // serviceProviderId?: number;
  serviceConsumerId?: number;
  profileImageURL?: string;
  username?: string;
  workEmailId?: string;
  roles?: Role[];
  firstName?: string;
  lastName?: string;
  contactNumber?: string;
  allowedServiceProviders?: ServiceProvider[];
  addresses?: UserAddress[];
}

export interface Role {
  roleId?: number;
  name: string;
  tenantId: string;
}

export interface UserAddress {
  address: string;
  creationDate?: Date;
  deactivated?: boolean;
  tenantId?: string;
  userAddressId?: string;
  userId?: string;
}
export interface ServiceConsumerResponse {
  serviceConsumerId?: number;
  creationDate?: Date;
  invoiceSummary?: string;
  name?: string;
  serviceprovider?: ServiceProvider[];

  serviceProviderId?: number;
  serviceProviderPk?: string;
  tenantId?: string;
  users?: User[];
  preferredServiceProviders?: ServiceProvider[];
}

export interface ServiceConsumerPreferredServiceProviderMapping {
  scspMappingId: number;
  preferredServiceProviderId: number;
  serviceConsumerId: number;
}

export interface ServiceConsumerServiceProviderUserMapping {
  scspUserMappingId: number;
  allowedSpId: number;
  defautUser: boolean;
  userId: number;
}
export interface ServiceProvider {
  name?: string;
  serviceProviderId?: number;
  serviceProviderPk?: string;
  tenantId?: string;
}
