export interface BookingCountStatistics {
  totalBookings: number,
  approvedBookings: number,
  rejectedBookings: number,
  pendingApprovalBookings: number,
  completedBookings: number,
  sailingBookings: number,
  rolloverRequestedBookings: number,
  transhipmentConnectedBookings: number
}
