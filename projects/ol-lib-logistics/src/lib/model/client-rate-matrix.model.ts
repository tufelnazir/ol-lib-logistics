export interface ClientRateMatrix {
  customerId?: number;
  polCode?: string;
  polName?: string;
  podCode?: string;
  podName?: string;
  t1Code?: string;
  t1Name?: string;
  t12Code?: string;
  t2Name?: string;
  remarks?: string;
  ldn20?: number;
  ldn40?: number;
  mty20?: number;
  mty40?: number;
  reeferSurcharge20Feet?: number;
  referredSurcharge20Feet?: number;
  imcoSurcharge20Feet?: number;
  imcoSurcharge40Feet?: number;
}
// export interface ClientRateMatrix {

//     customerId?: number;
//     polCode?: string;
//     polName?: string;
//     podCode?: string;
//     podName?: string;
//     t1Code?: string;
//     t1Name?: string;
//     t12Code?: string;
//     t2Name?: string;
//     remarks?: string;
//     Ldn20?: number;
//     Ldn40?: number;
//     Mty20?: number;
//     Mty40?: number;
//     ReeferSurcharge20Feet?: number;
//     referredSurcharge20Feet?: number;
//     ImcoSurcharge20Feet?: number;
//     ImcoSurcharge40Feet?: number;
// }
