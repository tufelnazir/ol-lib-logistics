export interface TrackingActivity {
  activitySequence?: number;
  activityName?: string;
  reached?:boolean;
  expectedActivityDateTime?: Date;
  actualActivityDateTime?: Date;
  delayedHrs?: number;
  vesselName?: string;
  voyageNumber?: string;
  present?:boolean;
  displayName?:string;
  displayValue?:string;


}
// export interface TrackingActivities {

//     activitySequence?: number ;
//       activityName?: string ;
//       reached?: boolean;
//       expectedActivityDateTime?: Date;
//       actualActivityDateTime?: Date;
//       delayedHrs?: TimeRanges;
//       vesselName?: string;
//       voyageNumber?:string;
// }
