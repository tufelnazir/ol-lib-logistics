export interface OverSizedContainerDetails {
  oversizedContainerQuantity?: number;
  containerType?: string;
  containerCategory?: string;
  length?: number;
  breadth?: number;
  height?: number;
  weight?: number;
  front?: number;
  back?: number;
  right?: number;
  left?: number;
}

// export interface OverSizedContainerDetails {

//     length?: number;
//     breadth?: number;
//     height?: number;
//     weight?: number;

// }
