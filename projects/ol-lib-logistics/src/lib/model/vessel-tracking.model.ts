import { TrackingActivity } from './tracking-activities.model';
export interface VesselTracking {
  bookingNumber?: string;
  containerNumber?: string;
  polCode?: string;
  polName?: string;
  podCode?: string;
  podName?: string;
  t1Code?: string;
  t1Name?: string;
  t2Code?: string;
  t2Name?: string;
  totalDelayedHrs?: Date;
  trackingActivities?: TrackingActivity[];
}

// import {TrackingActivities} from './tracking-activities.model'
//   import { format } from "path";
// export interface VesselTracking {

//     bookingNumber?: string;
//     containerNumber?: string;
//     polCode?: string;
//     polName?: string;
//     podCode?: string;
//     podName?: string;
//     t1Code?: string;
//     t1Name?: string;
//     t2Code?: string;
//     t2Name?: string;
//     totalDelayedHrs?: Date;
//     trackingActivities?: Array<TrackingActivities>;

//   }
