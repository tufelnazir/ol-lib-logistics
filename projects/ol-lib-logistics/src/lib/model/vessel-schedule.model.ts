import { Transhipment } from './transhipment.model';

export interface VesselSchedule {
  vesselScheduleId?: number;
  vesselName?: string; //MORINGIA
  voyageNumber?: string; //002E22
  polCode?: string; //INNSA
  polName?: string; //NHAVA SHEVA
  polTerminalCode?: string;
  polCountryCode?: string; //US
  podCode?: string; //OMSOH
  podName?: string; //SOHAR
  podTerminalCode?: string;
  polEtd?: Date; //13-Aug-2020 22:44
  podEta?: Date; //25-Sep-2020 14:48
  transitDays?: number; //43
  transitWeeks?: number; //6
  serviceName?: string; //del
  serviceCode?: string; // 01A1
  fromDate?: Date; // 13/8/20
  transhipments?: Array<Transhipment>;
}
// import { Transhipment } from "./transhipment.model"

// export interface VesselSchedule {
//     vesselName?: string; //MORINGIA
//     voyageNumber?: string; //002E22
//     polCode?: string; //INNSA
//     polName?: string; //NHAVA SHEVA
//     polTerminalCode?: string;
//     polCountryCode?: string; //US
//     podCode?: string;//OMSOH
//     podName?: string; //SOHAR
//     podTerminalCode?: string;
//     polEtd?: Date; //13-Aug-2020 22:44
//     podEta?: Date; //25-Sep-2020 14:48
//     transitDays?: string; //43
//     transitWeeks?: string; //6
//     serviceName?: string; //del
//     serviceCode?: string;// 01A1
//     fromDate?: Date; // 13/8/20
//     transhipments?:Array<Transhipment>
// }
