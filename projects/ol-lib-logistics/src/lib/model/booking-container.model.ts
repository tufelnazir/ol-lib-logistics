export interface BookingContainers {
  containerId?: number;
  containerNumber?: string;
  containerType?: string;
  containerCategory?: string;
  customSized?: true;
  length?: number;
  height?: number;
  breadth?: number;
  weigth?: number;
  quantity?: string;
  commodityCode?: string;
  commodityName?: string;
}
// export interface BookingContainers {

//     containerId?: number;
//     containerNumber?: string;
//     containerType?: string;
//     containerCategory?: string;
//     customSized?: true;
//     length?: number;
//     height?: number;
//     breadth?: number;
//     weigth?: number;
//     quantity?: string;
//     commodityCode?: string;
//     commodityName?: string;
// }
