export interface Invoice {
  invoiceNumber?: string;
  invoiceId?: 0;
  invoiceAmountPaid?: number;
  invoiceAmountPending?: number;
  totalInvoiceAmount?: number;
  slNumber?: string;
  partyName?: string;
  invoiceStatus?: string;
  invoicePdfUrl?: string;
}
