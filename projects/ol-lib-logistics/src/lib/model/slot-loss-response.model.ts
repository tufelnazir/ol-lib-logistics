export interface SlotLossResponse {
  totalTeuLost?: number;
  slotLossAmountPerTeu?: number;
  totalSlotLossAmount?: number;
  currencyCode?: string;
}
