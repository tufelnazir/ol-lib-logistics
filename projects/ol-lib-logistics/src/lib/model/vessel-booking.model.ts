import { BookingContainers } from './booking-container.model';

export interface VesselBooking {
  bookingId?: number;
  otmBookingId?: number;
  bookingNumber?: string;
  blNumber?: string;
  createdOn?: Date;
  bookingStatus?: string;
  paymentMode?: string;
  vesselName?: string;
  voyageNumber?: string;
  transitDays?: number;
  polCode?: string;
  polName?: string;
  podCode?: string;
  podName?: string;
  polEtd?: Date;
  podEta?: Date;
  t1PortCode?: string;
  t1PortName?: string;
  t1VesselName?: string;
  t1VoyageNumber?: string;
  t1transitDays?: number;
  t1Eta?: Date;
  t1Etd?: Date;
  t2PortCode?: string;
  t2PortName?: string;
  t2VesselName?: string;
  t2VoyageNumber?: string;
  t2transitDays?: number;
  t2Eta?: Date;
  t2Etd?: Date;
  serviceBlPdfUrl?: string;
  bookingContainer?: Array<BookingContainers>;
}
// import { BookingContainers } from "./booking-container.module";

// export interface VesselBooking {

//   bookingId?: number;
//   otmBookingId?: number;
//   bookingNumber?: string;
//   blNumber?: string;
//   createdOn?: Date;
//   bookingStatus?: string;
//   paymentMode?: string;
//   vesselName?: string;
//   voyageNumber?: string;
//   transitDays?: number;
//   polCode?: string;
//   polName?: string;
//   podCode?: string;
//   podName?: string;
//   polEtd?: string
//   podEta?: string;
//   t1PortCode?: string;
//   t1PortName?: string;
//   t1VesselName?: string;
//   t1VoyageNumber?: string;
//   t1transitDays?: number;
//   t1Eta?: string;
//   t1Etd?: string;
//   t2PortCode?: string;
//   t2PortName?: string;
//   t2VesselName?: string;
//   t2VoyageNumber?: string;
//   t2transitDays?: number;
//   t2Eta?: string;
//   t2Etd?: string;
//   bookingContainer?: Array<BookingContainers>;

// }
