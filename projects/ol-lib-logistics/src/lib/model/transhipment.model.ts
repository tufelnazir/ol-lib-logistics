export interface Transhipment {
  vesselScheduleId: number;
  transhipmentId?: number;
  tOrder?: number; // 1
  tVessel?: string; // null
  tVoyage?: string; // null
  tPolCode?: string; // null
  tPolName?: string; // null
  tPolTerminalCode?: string; // null
  tPolEta?: Date; // null
  tPolEtd?: Date; // null
  tPodCode?: string; // null
  tPodName?: string; // null
  tPodEta?: Date; // null
  tTransitDays?: string; // null
  tServiceName?: string; // null
  tServiceCode?: string; // null
} // export interface Transhipment {
//     tOrder?: number; // 1
//     tVessel?: string;// null
//     tVoyage?: string;// null
//     tPolCode?: string;// null
//     tPolName?: string;// null
//     tPolTerminalCode?:string;// null
//     tPolEta?: Date;// null
//     tPolEtd?: Date;// null
//     tPodCode?: string; // null
//     tPodName?: string; // null
//     tPodEta?: Date; // null
//     tTransitDays?: string; // null
//     tServiceName?: string; // null
//     tServiceCode?: string; // null
// }
