export interface HazCargoAcceptanceRequest {
  imcoClass?: string;
  unNumber?: string;
  vesselName?: string;
}

export interface HazCargoAcceptanceResponse {
  accepted: boolean;
  message: string;
}

// export interface HazCargoAcceptance {
//
//     customerId?: number;
//   imcoClass?: string;
//   unNumber?: string;
//   accepted?: true;
//   message?: string;

// }
