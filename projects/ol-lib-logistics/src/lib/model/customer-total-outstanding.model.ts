export interface InvoiceSummary {


  allInvoices?:number;
  paidInvoices?:number;
  unpaidInvoices?:number;
  partiallyPaidInvoices?:number;


  // the following properties may not be needed
  customerId?: number;

  totalSpent: string;

  // pending invoices
  totalUnpaidInvoices?: number;

  // pending amount
  totalUnpaidAmount?: string;

  //over due invoices
  totalOverdueInvoices?: number;

  // over due amount
  totalOverdueAmount?: string;
}
// export interface CustomerTotalOutstanding {

//     customerId?: number;
//     totalUnpaidInvoices?: string;
//     totalUnpaidAmount?: number;
//     totalOverdueInvoices?: string;
//     totalOverdueAmount?: number;

// }
