export interface Country {
  countryId?: number;
  countryCode?: string;
  countryName?: string;
  twServing?: boolean;
  createdOn?: Date;
}
// export interface Country {

//     countryId?: number;
//     countryCode?: string;
//     countryName?: string;
//     twServing?: boolean;
//     createdOn?: Date;
//   }
