import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { map } from "rxjs/operators";
import { User, UserAddress } from "../model/user.model";

const userManagementRoutes = {
  // each route is for 1 API call
  getUsers: () => `/im/apea/security/users`,
  getCurrentUser: () => `/im/apea/security/get-current-user`,
  getCurrentUserAddresses: () => `/v1/current-user-addresses`,
  createUserAddress: () => `/v1/user-addresses`,
  updateUserAddress: (userAddressId: number) => `/v1/user-addresses?userAddressId=${userAddressId}`,
};

@Injectable({
  providedIn: "root",
})
export class UserManagementService {
  constructor(private httpClient: HttpClient) {}

  getUsers(): Observable<User[]> {
    let $users: Observable<User[]> = this.httpClient
      .get(userManagementRoutes.getUsers())
      .pipe(
        map((x) => {
          return x["content"] as User[];
        })
      );

    return $users;
  }

  getCurrentUser(): Observable<User> {
    let $user: Observable<User> = this.httpClient
      .get(userManagementRoutes.getCurrentUser())
      .pipe(
        map((x) => {
          return x as User;
        })
      );

    return $user;
  }

  getCurrentUserAddresses(): Observable<UserAddress[]> {
    let $userAddresses: Observable<UserAddress[]> = this.httpClient
      .get(userManagementRoutes.getCurrentUserAddresses())
      .pipe(
        map((x) => {
          return x as UserAddress[];
        })
      );
    return $userAddresses;
  }

  createUserAddress(userAddress: UserAddress): Observable<UserAddress> {
    let $userAddress: Observable<UserAddress> = this.httpClient
      .post(userManagementRoutes.createUserAddress(), userAddress)
      .pipe(
        map((x) => {
          return x as UserAddress;
        })
      );

    return $userAddress;
  }

  updateUserAddress(userAddressId: number, userAddress: UserAddress): Observable<UserAddress> {


    let api = userManagementRoutes.updateUserAddress(userAddressId);
    console.log("Update User API : ", api);

    let $userAddress: Observable<UserAddress> = this.httpClient
      .put(userManagementRoutes.updateUserAddress(userAddressId), userAddress)
      .pipe(
        map((x) => {
          return x as UserAddress;
        })
      );
    return $userAddress;
  }
}
