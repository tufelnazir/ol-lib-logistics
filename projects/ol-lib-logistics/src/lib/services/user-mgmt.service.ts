import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Role, User } from "../model/user.model";

const userMgmtRoutes = {
  getUsers: (firstName: string) => `/im/apea/security/users-by-name?firstName=${firstName}`,

  createUser: () => `/im/apea/security/users`,
  updateUser: (userId:number) => `/im/apea/security/users/${userId}`,
  patchUser: (userId: number) => `/im/apea/security/users/${userId}`,
};

@Injectable({
  providedIn: "root",
})
export class UserMgmtService {
  constructor(private httpClient: HttpClient) {}

  getUsers(firstName?: string): Observable<User[]> {
    if (!firstName) {
      firstName = "";
    }
    let $users: Observable<User[]> = this.httpClient.get(userMgmtRoutes.getUsers(firstName)).pipe(
      map((x) => {
        return x["content"] as User[];
      })
    );

    return $users;
  }

  createUser(user: User): Observable<User> {
    let $userResponse: Observable<User> = this.httpClient.post(userMgmtRoutes.createUser(), user).pipe(
      map((x) => {
        return x as User;
      })
    );

    return $userResponse;
  }

  /**
   * Only username, firstname and lastname will be updated
   * @param userId
   * @param user
   */
  updateUser(userId: number, user: User): Observable<User> {
    let $userResponse: Observable<User> = this.httpClient.put(userMgmtRoutes.updateUser(userId), user).pipe(
      map((x) => {
        return x as User;
      })
    );

    return $userResponse;
  }

  activateUser(userId: number): Observable<User> {
    let patchUser: User = {
      userId: userId,
      patchField: "status",
      enabled: true,
    };

    let $userResponse: Observable<User> = this.httpClient.patch(userMgmtRoutes.patchUser(userId), patchUser).pipe(
      map((x) => {
        return x as User;
      })
    );

    return $userResponse;
  }

  deactivateUser(userId: number): Observable<User> {
    let patchUser: User = {
      userId: userId,
      patchField: "status",
      enabled: false,
    };

    let $userResponse: Observable<User> = this.httpClient.patch(userMgmtRoutes.patchUser(userId), patchUser).pipe(
      map((x) => {
        return x as User;
      })
    );

    return $userResponse;
  }

  changePassword(userId: number, newPassword: string): Observable<User> {
    let patchUser: User = {
      userId: userId,
      patchField: "password",
      newPassword: newPassword,
    };

    let $userResponse: Observable<User> = this.httpClient.patch(userMgmtRoutes.patchUser(userId), patchUser).pipe(
      map((x) => {
        return x as User;
      })
    );

    return $userResponse;
  }

  updateRoles(userId: number, newRoles: Role[]): Observable<User> {
    let patchUser: User = {
      userId: userId,
      patchField: "roles",
      roles: newRoles,
    };

    let $userResponse: Observable<User> = this.httpClient.patch(userMgmtRoutes.patchUser(userId), patchUser).pipe(
      map((x) => {
        return x as User;
      })
    );

    return $userResponse;
  }
}
