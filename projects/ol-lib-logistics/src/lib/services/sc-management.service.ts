import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import {
  Role,
  ServiceConsumerPreferredServiceProviderMapping,
  ServiceConsumerServiceProviderUserMapping,
  ServiceProvider,
  User,
} from "../model/user.model";

export interface ServiceConsumerAPIContext {
  filter?: string;
  page?: number;
  size?: number;
}

const scMgmtRoutes = {
  // each route is for 1 API call
  getServiceConsumers: (x: ServiceConsumerAPIContext) =>
    `/v1/service-consumers?name-filter=${x.filter}&page=${x.page}&size${x.size}`,

  getServiceProviders: () => `/v1/service-providers`,
  removeServiceProviderFromServiceConsumer: (serviceConsumerId: number, serviceProviderId: number) =>
    `/v1/service-consumers-service-provider-remove/${serviceConsumerId}?serviceProviderId=${serviceProviderId}`,

  addServiceProviderToServiceConsumer: (serviceConsumerId: number, serviceProviderId: number) =>
    `/v1/service-consumers-service-provider-add/${serviceConsumerId}?serviceProviderId=${serviceProviderId}`,

  addUserToServiceConsumer: (serviceConsumerId: number, userId: number) =>
    `/v1/service-consumers-user-associate/${serviceConsumerId}?userId=${userId}`,
  removeUserFromServiceConsumer: (serviceConsumerId: number, userId: number) =>
    `/v1/service-consumers-user-disassociate/${serviceConsumerId}?userId=${userId}`,

  addAllowedSPToServiceConsumerUser: (serviceConsumerId: number, userId: number, serviceProviderId: number) =>
    `/v1/service-consumers-allowed-sp-user-add/${serviceConsumerId}?userId=${userId}&serviceProviderId=${serviceProviderId}`,
  removeAllowedSPFromServiceConsumerUser: (serviceConsumerId: number, userId: number, serviceProviderId: number) =>
    `/v1/service-consumers-allowed-sp-user-remove/${serviceConsumerId}?userId=${userId}&serviceProviderId=${serviceProviderId}`,
};

@Injectable({
  providedIn: "root",
})
export class ServiceConsumerManagementService {
  constructor(private httpClient: HttpClient) {}


  getServiceConsumers(params: ServiceConsumerAPIContext): Observable<any> {
    if (!params.filter) {
      params.filter = "";
    }
    if (!params.page) {
      params.page = 0;
    }
    if (params.size) {
      params.size = 10;
    }
    let $serviceConsumers: Observable<any> = this.httpClient.get(scMgmtRoutes.getServiceConsumers(params));
    return $serviceConsumers;
  }

  getServiceProviders() {
    let $serviceProviders: Observable<ServiceProvider[]> = this.httpClient
      .get(scMgmtRoutes.getServiceProviders())
      .pipe(
        map((x) => {
          return x as ServiceProvider[];
        })
      );
    return $serviceProviders;
  }

  removeServiceProviderFromServiceConsumer(serviceConsumerId: number, serviceProviderId: number): Observable<any> {
    console.log("sending ", serviceConsumerId + " -- " + serviceProviderId);
    return this.httpClient.post(
      scMgmtRoutes.removeServiceProviderFromServiceConsumer(serviceConsumerId, serviceProviderId),
      {}
    );
  }
  addServiceProviderToServiceConsumer(
    serviceConsumerId: number,
    serviceProviderId: number
  ): Observable<ServiceConsumerPreferredServiceProviderMapping> {
    console.log("sending ", serviceConsumerId + " -- " + serviceProviderId);
    return this.httpClient
      .post(scMgmtRoutes.addServiceProviderToServiceConsumer(serviceConsumerId, serviceProviderId), {})
      .pipe(
        map((x) => {
          return x as ServiceConsumerPreferredServiceProviderMapping;
        })
      );
  }

  addUserToServiceConsumer(serviceConsumerId: number, userId: number): Observable<User> {
    let $user = this.httpClient.post(scMgmtRoutes.addUserToServiceConsumer(serviceConsumerId, userId), {}).pipe(
      map((x) => {
        return x as User;
      })
    );

    return $user;
  }

  removeUserFromServiceConsumer(serviceConsumerId: number, userId: number): Observable<User> {
    let $user = this.httpClient
      .post(scMgmtRoutes.removeUserFromServiceConsumer(serviceConsumerId, userId), {})
      .pipe(
        map((x) => {
          return x as User;
        })
      );

    return $user;
  }

  addAllowedSPToServiceConsumerUser(
    serviceConsumerId: number,
    userId: number,
    serviceProviderId: number
  ): Observable<ServiceConsumerServiceProviderUserMapping> {
    let $allowedSPMapping = this.httpClient
      .post(scMgmtRoutes.addAllowedSPToServiceConsumerUser(serviceConsumerId, userId, serviceProviderId), {})
      .pipe(
        map((x) => {
          return x as ServiceConsumerServiceProviderUserMapping;
        })
      );

    return $allowedSPMapping;
  }

  removeAllowedSPFromServiceConsumerUser(
    serviceConsumerId: number,
    userId: number,
    serviceProviderId: number
  ): Observable<any> {
    let $allowedSPMapping = this.httpClient
      .post(
        scMgmtRoutes.removeAllowedSPFromServiceConsumerUser(serviceConsumerId, userId, serviceProviderId),
        {}
      )
      .pipe(
        map((x) => {
          return x;
        })
      );

    return $allowedSPMapping;
  }

}
