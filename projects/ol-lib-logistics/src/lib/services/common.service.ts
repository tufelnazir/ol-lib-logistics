import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { map } from "rxjs/operators";
import { ClientRateMatrix } from "../model/client-rate-matrix.model";
import { Commodity } from "../model/commodity.model";
import { Container } from "../model/container.model";
import { Country } from "../model/country.model";
import { Currency } from "../model/currency.model";
import { InvoiceSummary } from "../model/customer-total-outstanding.model";
import {
  HazCargoAcceptanceRequest,
  HazCargoAcceptanceResponse,
} from "../model/haz-cargo-acceptance.model";
import {DataBuilder} from '../model/data-builder'
import { Invoice } from "../model/invoice.model";
import { Port } from "../model/port.model";
import { SlotLossResponse } from "../model/slot-loss-response.model";
import { SlotLossRequest } from "../model/slot-loss-request.model";
import { VesselBooking } from "../model/vessel-booking.model";
import { VesselSchedule } from "../model/vessel-schedule.model";
import { VesselTracking } from "../model/vessel-tracking.model";
import { DatePipe } from "@angular/common";
import { VesselBookingStatistics } from "../model/vessel-booking-statistics.model";
import { InvoiceCountStatistics } from "../model/invoice-count-statistics.model";
import { BookingCountStatistics } from "../model/booking-count-statistics.model";
import { ClientRateMatrixRequest } from "../model/client-rate-matrix-request.model";
import { User } from "../model/user.model";

export interface VesselBookingAPIContext {
  customerId: number;
}

/**
 * public static final String PENDING = "Pending";
	//public static final String PARTIAL_PAID = "Partial Paid";
	public static final String PAID = "Paid";
	public static final String OVERDUE = "Overdue";
	public static final String PARTIAL_PENDING_OVER_DUE = "Partial Pending Overdue";
	
	public static final String ALL_INVOICES = "All";
	public static final String PARTIALLY_PAID_INVOICES = "Partly Paid";
	public static final String UNPAID_INVOICES = "Pending";
	
 */
export enum InvoiceFilterOptions {
  all = "All",
  paid = "Fully Paid",
  unpaid = "Pending",
  partiallyPaid = "Partly Paid",
}

// "totalInvoices": 0,
// "totalPaid": 0,
// "totalPartialPaid": 0,
// "totalUnpaid": 0

/**
 * All, Pending Approval, Approved,
 * Rejected, Cancelled,
 * Transhipment Connected,
 *  Pending Rollover Approval, Sailing]
 */
export enum BookingsFilterOptions {
  all = "All",
  sailing = "Sailing",
  approved = "Approved",
  rejected = "Rejected",
  pendingApproval = "Pending Approval",
  completed = "Completed",
  rollover = "Pending Rollover Approval",
  cancelled = "CANCELLED",
  transhipmentConnectedBookings = "Transhipment Connected",
}

const invoiceRoutes = {
  // getInvoices: () => `/v1/invoices`,
  getInvoiceSummary: () => `/v1/invoice-statistics`,
  getFilteredInvoices: (filterOption) =>
    `/v1/paged-invoices?invoiceStatus=${filterOption}`,
  getInvoiceCountStatistics: () => `/v1/invoice-count-statistics`,
};

const bookingRoutes = {
  getVesselBookings: () => `/v1/slot-bookings`,
  getBookingSummary: () => "/v1/slot-bookings-statistics",
  getFilteredBookings: (filterOption) =>
    `/v1/paged-slot-bookings?bookingStatus=${filterOption}`,
  getServiceBL: (bookingNumber) =>
    `/v1/slot-bookings/${bookingNumber}/service-bl`,
  getBookingCountStatistics: () => `/v1/slot-bookings-count-statistics`,
};

const vesselTrackingRoutes = {
  getVesselTracking: (bookingNumber) =>
    `/v1/vessel-tracking?bookingNumber=${bookingNumber}`,
};

const vesselScheduleRoutes = {
  getVesselSchedules: (fromPort, toPort, fromDate) =>
    `/v1/vessel-schedules?fromPort=${fromPort}&toPort=${toPort}&fromDate=${fromDate}`,
};

const commonRoutes = {
  // each route is for 1 API call
  getPorts: () => `/v1/from-ports`,

  // POST API which will have SlotLossRequestVO sent in the body of the request
  submitSlotLossRequest: () => `/v1/slot-loss-estimate`,

  // POST  API : please provide the RequestVO & ResponseVO classes
  getClientRateMatrix: () => "/v1/client-rate-matrix",

  // POST API : please provide the RequestVO & Response VO classes
  getHazCargoAcceptance: () => `/v1/haz-cargo-acceptance-enquiry`,

  // GET : last priority
  getCommodities: () => `/v1/commodities`,

  // GET last priority
  getContainers: () => `/v1/containers`,

  // GET last priority
  getCountries: () => `/v1/countries`,

  // GET last priority
  getCurrencies: () => `/v1/curriencies`,

  // GET last priority
  getUser: () => `/v1/users`,
};

@Injectable({
  providedIn: "root",
})
export class CommonService {
  constructor(private httpClient: HttpClient) {}

  getServiceBL(bookingNumber: string): Observable<any> {
    let $serviceBL: Observable<any> = this.httpClient
      .get(bookingRoutes.getServiceBL(bookingNumber))
      .pipe(
        map((x) => {
          return x;
        })
      );
    return $serviceBL;
  }
  getFilteredInvoices(option: InvoiceFilterOptions): Observable<Invoice[]> {
    let $filteredInvoices: Observable<Invoice[]> = this.httpClient
      .get(invoiceRoutes.getFilteredInvoices(option.toString()))
      .pipe(
        map((x) => {
          return x as Invoice[];
        })
      );

    return $filteredInvoices;
  }

  getInvoiceCountStatistics(): Observable<InvoiceCountStatistics> {
    let $countStatistics: Observable<InvoiceCountStatistics> = this.httpClient
      .get(invoiceRoutes.getInvoiceCountStatistics())
      .pipe(
        map((x) => {
          return x as InvoiceCountStatistics;
        })
      );
    return $countStatistics;
  }

  getFilteredAllInvoices(): Observable<Invoice[]> {
    // let $vesselInvoice: Observable<Invoice[]> = of(DataBuilder.buildInvoices());
    let $vesselInvoice: Observable<Invoice[]> = this.httpClient
      .get(invoiceRoutes.getFilteredInvoices(InvoiceFilterOptions.all))
      .pipe(
        map((x) => {
          return x as Invoice[];
        })
      );

    return $vesselInvoice;
  }

  getInvoiceSummary(): Observable<InvoiceSummary> {
    let $invoiceSummary: Observable<InvoiceSummary> = this.httpClient
      .get(invoiceRoutes.getInvoiceSummary())
      .pipe(
        map((x) => {
          return x as InvoiceSummary;
        })
      );

    return $invoiceSummary;
  }

  getBookingSummary(): Observable<VesselBookingStatistics> {
    let $bookingSummary: Observable<VesselBookingStatistics> = this.httpClient
      .get(bookingRoutes.getBookingSummary())
      .pipe(
        map((x) => {
          return x as VesselBookingStatistics;
        })
      );

    return $bookingSummary;
  }

  getFilteredBookings(
    filterOption: BookingsFilterOptions
  ): Observable<VesselBooking[]> {
    // let filterOptionString = filterOption.toUpperCase();
    let url = bookingRoutes.getFilteredBookings(filterOption);
    console.log("The URL :" + url);

    let $filteredBookings: Observable<VesselBooking[]> = this.httpClient
      .get(url)
      .pipe(
        map((x) => {
          return x as VesselBooking[];
        })
      );

    return $filteredBookings;
  }

  /**
   * 
   * ttotalBookings,
  totalPendingAproval,
  totalApproved,
  totalCurrentlySailing,
  totalCompleted,
  totalRollover,
  totalRejected
  getInvoiceCountStatistics(): Observable<InvoiceCountStatistics> {
    let $countStatistics: Observable<InvoiceCountStatistics> = this.httpClient
      .get(invoiceRoutes.getInvoiceCountStatistics())
      .pipe(
        map((x) => {
          return x as InvoiceCountStatistics;
        })
      );
    return $countStatistics;
  }
   * 
   */
  getBookingsCountStatistics(): Observable<BookingCountStatistics> {
    let $countStatistics: Observable<BookingCountStatistics> = this.httpClient
    .get(bookingRoutes.getBookingCountStatistics())
    .pipe(
      map((x) => {
        return x as BookingCountStatistics;
      }) 
    );

    return $countStatistics;
  }

  getVesselBookings(): Observable<VesselBooking[]> {
    // let $vesselBookings: Observable<VesselBooking[]> = of(
    //   DataBuilder.buildVesselBooking()
    // );

    let $vesselBookings: Observable<VesselBooking[]> = this.httpClient
      .get(bookingRoutes.getVesselBookings())
      .pipe(
        map((x) => {
          return x as VesselBooking[];
        })
      );

    return $vesselBookings;
  }

  getVesselTracking(bookingNumber: string): Observable<VesselTracking> {
    // let $vesselTracking: Observable<VesselTracking> = of(
    //   DataBuilder.buildVesselTracking()
    // );
    let $vesselTracking: Observable<VesselTracking> = this.httpClient
      .get(vesselTrackingRoutes.getVesselTracking(bookingNumber))
      .pipe(
        map((x) => {
          return x as VesselTracking;
        })
      );

    return $vesselTracking;
  }

  getVesselSchedules(
    fromPort: string,
    toPort: string,
    fromDate: Date
  ): Observable<VesselSchedule[]> {
    // let $vesselSchedule: Observable<VesselSchedule[]> = of(
    //   DataBuilder.buildVesselSchedules()
    // );

    //  dd-MMM-yyyy
    let datePipe: DatePipe = new DatePipe("en-US");
    let stringDate = datePipe.transform(fromDate, "dd-MMM-yyyy");

    let $vesselSchedule: Observable<VesselSchedule[]> = this.httpClient
      .get(
        vesselScheduleRoutes.getVesselSchedules(fromPort, toPort, stringDate)
      )
      .pipe(
        map((x) => {
          return x as VesselSchedule[];
        })
      );

    return $vesselSchedule;
  }
  getPorts(): Observable<Port[]> {
    // let $vesselPort: Observable<Port[]> = null ; //of(DataBuilder.buildPorts());

    let $vesselPort = this.httpClient.get(commonRoutes.getPorts()).pipe(
      map((response) => {
        return response as Port[];
      })
    );

    return $vesselPort;
  }

  submitSlotLossRequest(
    slotLossRequest: SlotLossRequest
  ): Observable<SlotLossResponse> {
    let $vesselSlotLoss: Observable<SlotLossResponse> = this.httpClient
      .post(commonRoutes.submitSlotLossRequest(), slotLossRequest)
      .pipe(
        map((x) => {
          return x as SlotLossResponse;
        })
      );

    return $vesselSlotLoss;
  }
  getClientRateMatrix(
    clientRateMatrixRequest: ClientRateMatrixRequest
  ): Observable<ClientRateMatrix> {
    let $vesselClientRateMatrix: Observable<
      ClientRateMatrix
    > = this.httpClient
      .post(commonRoutes.getClientRateMatrix(), clientRateMatrixRequest)
      .pipe(
        map((x) => {
          return x as ClientRateMatrix;
        })
      );
    return $vesselClientRateMatrix;
  }

  getHazCargoAcceptance(
    hazCargoRequest: HazCargoAcceptanceRequest
  ): Observable<HazCargoAcceptanceResponse> {
    let $vesselHazCargoAcceptance: Observable<HazCargoAcceptanceResponse> = this.httpClient
      .post(commonRoutes.getHazCargoAcceptance(), hazCargoRequest)
      .pipe(
        map((x) => {
          return x as HazCargoAcceptanceResponse;
        })
      );
    return $vesselHazCargoAcceptance;
  }

  getCommodities(): Observable<Commodity[]> {
    let $vesselcommodity: Observable<Commodity[]> = of(
      DataBuilder.buildCommodity()
    );
    return $vesselcommodity;
  }

  getContainers(): Observable<Container[]> {
    let $vesselcontainers: Observable<Container[]> = of(
      DataBuilder.buildContainer()
    );
    return $vesselcontainers;
  }

  getCountries(): Observable<Country[]> {
    let $vesselcountries: Observable<Country[]> = of(
      DataBuilder.buildCountry()
    );
    return $vesselcountries;
  }
  getCurrencies(): Observable<Currency[]> {
    let $vesselcurrency: Observable<Currency[]> = of(
      DataBuilder.buildCurrency()
    );
    return $vesselcurrency;
  }
}
