import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";

export interface ToolbarState {
  title: string;
}

@Injectable({
  providedIn: "root",
})
export class ToolbarStateService {
  public currentToolbarState: Observable<ToolbarState>;

  private toolbarState = new BehaviorSubject<ToolbarState>({
    title: "Vessel Portal",
  });

  constructor() {
    this.currentToolbarState = this.toolbarState.asObservable();
  }

  updateTitle(newTitle: string) {
    this.toolbarState.next({
      title: newTitle,
    });
  }
}
